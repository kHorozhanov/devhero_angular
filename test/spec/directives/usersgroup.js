'use strict';

describe('Directive: usersGroup', function () {

  // load the directive's module
  beforeEach(module('devheroApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<users-group></users-group>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the usersGroup directive');
  }));
});
