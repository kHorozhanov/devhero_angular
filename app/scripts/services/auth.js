'use strict';

/**
 * @ngdoc service
 * @name devheroApp.auth
 * @description
 * # auth
 * Factory in the devheroApp.
 */
angular.module('devheroApp')
  .factory('auth', function () {
    return {
      // Add authorization token to headers
      request: function (config) {
          config.headers = config.headers || {};
          var token = "Basic ZGV2OmZqZW1iYTcx";
          config.headers['Authorization'] = token;
          
          return config;
      }
    };
  });
