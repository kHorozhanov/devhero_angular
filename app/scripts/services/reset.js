'use strict';

/**
 * @ngdoc service
 * @name devheroApp.reset
 * @description
 * # reset
 * Factory in the devheroApp.
 */
angular.module('devheroApp')
  .factory('reset', function ($localStorage) {

  	var defaultValues = {
  		limit: '10',
  		page: 1,
  		good: 0,
      alert: true,
      city: '',
      experience: '',
      q: '',
      sklf: 0,
      slrf: 0,
      expf: 0,
      sort: '',
      salary: '',
      en: false,
      rw: false,
      grp: false,
      pubDate: {
        startDate: '',
        endDate: ''
      },
      lastVisit: {
        startDate: '',
        endDate: ''
      },
      skills: []
  	}

    return {
    	getDefault: function(){
    		return defaultValues;
    	},
    	resetStorage: function(){
    		$localStorage.$reset(defaultValues);
    	}
    };
  });
