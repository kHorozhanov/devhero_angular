'use strict';

/**
 * @ngdoc function
 * @name devheroApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the devheroApp
 */
angular.module('devheroApp')
  .controller('MainCtrl', function ($scope, $localStorage, $http, $filter, $timeout, $sce) {
    $scope.storage = $localStorage;

    function createUrl(storage){
    	var url = '';

    	for (var key in storage){
    		if (typeof storage[key] != 'object' && typeof storage[key] != 'function'){
    			if (typeof storage[key] === 'boolean'){
    				url+= key + '=' + Number(storage[key]) + '&';
    			} else{
    				url+= key + '=' + storage[key] + '&';
    			}
    		}
    	}

    	var skillsEdited = [];

    	storage.skills.forEach(function(value){
    		skillsEdited.push(value.text);
    	});

    	url+='skills=' + skillsEdited + '&';
    	url+='cr1=' + storage.pubDate.startDate + '&';
    	url+='cr2=' + storage.pubDate.endDate + '&';
    	url+='lv1=' + storage.lastVisit.startDate + '&';
    	url+='lv2=' + storage.lastVisit.endDate + '&';

    	//console.log(url);

    	return url;
    }

    function watchNeeds(){
    	$timeout(function(){
    		requestInProgress = false;
		    if (needRequest) {
		    	requestInProgress = true;
		    	needRequest = false;
		    	request();
		    };
    	}, 500);
    }

    function request(){
    	requestInProgress = true;
        $scope.dataLoaded = false;
    	var url = createUrl($scope.storage);
    		$http.get('http://devhero.club:3000/api/users?' + url)
	    		.then(function(data){
                    $scope.dataLoaded = true;
	    			$scope.data = data.data;
	    			$scope.data.pages = Math.ceil($scope.data.count/$scope.data.limit);
	    			console.log($scope.data.users[0]);

	    			$scope.selectedUsers = [];
    				$scope.selectAll = false;

	    			watchNeeds();

                    console.log($scope.data);

                    $scope.data.users.forEach(function(value){
                        value.salary += 2000;
                    })
	    		}, function(error){
	    			//console.error(error);
	    			watchNeeds();
	    		});
    }

    var requestInProgress = false;
    var needRequest = false;

    $scope.$watch('storage', function(newValue, oldValue){
        $scope.q = $scope.storage.q;
        $scope.city = $scope.storage.city;
        $scope.salary = $scope.storage.salary;
        $scope.experience = $scope.storage.experience;

    	if (newValue.page == oldValue.page && newValue.page != 1){
    		$scope.storage.page = 1;
    		return false;
    	}
    	if(!requestInProgress){
    		request();
    	} else{
    		needRequest = true;
    	}
    }, true);

    $scope.selectedUsers = [];
    $scope.selectAll = false;

    $scope.$watchCollection('selectedUsers', function(){
    	if ($scope.data){
    		var userCount = 0;
    		if (!$scope.storage.grp){
    			userCount = $scope.data.users.length;
    		} else{
    			$scope.data.users.forEach(function(city){
    				city.u.forEach(function(user){
    					userCount++;
    				})
    			})
    		}
    		if ($scope.selectedUsers.length == userCount){
		    	$scope.selectAll = true;
		    } else{
		    	$scope.selectAll = false;
		    }
    	}
    	
    })

    $scope.changeAllSelected = function(state){
    	$scope.selectedUsers = [];

    	if (!$scope.storage.grp){

    		if (state){	
	    		$scope.data.users.forEach(function(user){
	    			user.selected = true;
	    			$scope.selectedUsers.push(user);
	    		});
	    	} else {
	    		$scope.data.users.forEach(function(user){
	    			user.selected = false;
	    		});
	    	}
    	} else{
    		if (state){
    			$scope.data.users.forEach(function(city){
    				city.u.forEach(function(user){
    					user.selected = true;
    					$scope.selectedUsers.push(user);
    				})
    			})
    		} else{
    			$scope.data.users.forEach(function(city){
    				city.u.forEach(function(user){
    					user.selected = false;
    				})
    			})
    		}
    	}
    }

    $scope.addToSelected = function(user){
    	if (user.selected){
    		$scope.selectedUsers.push(user);
    	} else{
    		$scope.selectedUsers.splice($scope.selectedUsers.indexOf(user), 1);
    	}
    }

    $scope.setUserGood = function(good, user, index, cityIndex){
    	if (!angular.isUndefined(good) && !angular.isUndefined(user) && !angular.isUndefined(index)){
    		$http.post('http://devhero.club:3000/api/users/ch', {
	    		uid: user._id,
	    		good: good
	    	}).then(function(success){
	    		if (!angular.isUndefined(cityIndex)){
	    			$scope.data.users[cityIndex].u.splice(index, 1);
	    		} else{
	    			$scope.data.users.splice(index, 1);
	    		}
	    		$scope.selectedUsers.splice($scope.selectedUsers.indexOf(user), 1);
	    	}, function(error){
	    		console.error(error);
	    	})
    	}
    }

    $scope.getCurrentPercent = function(salary){
        $scope.salaryPercent = salary/1500 * 100;

        if ($scope.salaryPercent >= 100) $scope.salaryPercent = 100;

        if (salary <= 500) {
          $scope.salaryColor = '#ade65f';
        } else if (salary > 500 && salary <= 1000){
          $scope.salaryColor = '#ea914e';
        } else $scope.salaryColor = '#ec2f2e';
    }

    $scope.getCurrentExp = function(exp){
        if (exp <= 3) {
          $scope.expColor = '#ec3933';
        } else if (exp > 3 && exp <= 5){
          $scope.expColor = '#eac34e';
        } else $scope.expColor = '#75d2a0';

        $scope.expPercent = exp * 10;
    }

    $scope.createMarkUp = function(user){
        $scope.general = $sce.trustAsHtml(user.profile);
        $scope.looking = $sce.trustAsHtml(user.lookingfor);
        $scope.general = $sce.trustAsHtml(user.highlights);
    }

    $scope.setMultiGood = function(value){

        if (!$scope.selectedUsers.length){
            alert('Choose users before');
            return false;
        } else{
            var array = [];

            $scope.selectedUsers.forEach(function(value){
                array.push(value._id);
            })

            $http.post('http://devhero.club:3000/api/users/mch', {
                uids: JSON.stringify(array),
                good: value
            }).then(function(success){

            }, function(error){
                console.error(error);
            })
            console.log(array);
            request();
        }
        console.log(value);
    }

    $scope.blur_request = function(key, value){
        $scope.storage[key] = value;
    }

    $scope.sendUserData = function(user, model, key){
        var obj = {};
        obj[key] = model;
        console.log(obj);
        $http.post('http://devhero.club:3000/api/user/' + user._id, obj)
    }

    $scope.selectOptions = [1,2,3,4,5];

  });
