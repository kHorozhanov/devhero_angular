'use strict';

/**
 * @ngdoc function
 * @name devheroApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the devheroApp
 */
angular.module('devheroApp')
  .controller('NavbarCtrl', function ($scope, $localStorage, reset, $filter) {
    $scope.storage = $localStorage;
    $scope.$watch('storage', function(){
    	$localStorage = $scope.storage;
    }, true);

    $scope.changeState = function(state){
    	switch ($scope.storage[state]){
    		case 0:
    			$scope.storage[state] = 1;
    			break;
    		case 1:
    			$scope.storage[state] = -1;
    			break;
    		default: 
    			$scope.storage[state] = 0;
    	}
    };

    $scope.paginateDec = function(){
    	if ($scope.storage.page > 1) {
    		$scope.storage.page --;
    	} else{
    		$scope.storage.page = 1;
    	}
    };

    $scope.paginateInc = function(data, page){
    	if (data.pages > $scope.storage.page){
    		$scope.storage.page++;
    	}
    };

    $scope.resetStorage = function(){
    	reset.resetStorage();
    };

    $scope.lastVisit = $scope.storage.lastVisit;
    $scope.pubDate = $scope.storage.pubDate;

    $scope.$watch('lastVisit', function(){
    	if ($scope.lastVisit.startDate && $scope.lastVisit.endDate){
	    	$scope.storage.lastVisit.startDate = $filter('date')(new Date($scope.lastVisit.startDate), 'yyyy-MM-dd');
	    	$scope.storage.lastVisit.endDate = $filter('date')(new Date($scope.lastVisit.endDate), 'yyyy-MM-dd');
    	}
    }, true);

    $scope.$watch('pubDate', function(){
    	if ($scope.pubDate.startDate && $scope.pubDate.endDate){
    		$scope.storage.pubDate.startDate = $filter('date')(new Date($scope.pubDate.startDate), 'yyyy-MM-dd');
    		$scope.storage.pubDate.endDate = $filter('date')(new Date($scope.pubDate.endDate), 'yyyy-MM-dd');
    	}
    }, true);

  });
