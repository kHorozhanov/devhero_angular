'use strict';

/**
 * @ngdoc overview
 * @name devheroApp
 * @description
 * # devheroApp
 *
 * Main module of the application.
 */
angular
  .module('devheroApp', [
    'ngAnimate',
    'ngStorage',
    'ngSanitize',
    'ngTagsInput',
    'daterangepicker',
    'angular-svg-round-progressbar',
    'ngAnimate'
  ])
  .config(function($httpProvider){
    $httpProvider.interceptors.push('auth');
  })
  .run(function($localStorage, reset){
  		$localStorage.$default(reset.getDefault());
  });
