'use strict';

/**
 * @ngdoc directive
 * @name devheroApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('devheroApp')
  .directive('navbar', function () {
    return {
      templateUrl: 'views/navbar.html',
      restrict: 'E',
      controller: 'NavbarCtrl'
    };
  });
