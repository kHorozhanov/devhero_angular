'use strict';

/**
 * @ngdoc directive
 * @name devheroApp.directive:usersGroup
 * @description
 * # usersGroup
 */
angular.module('devheroApp')
  .directive('usersGroup', function () {
    return {
      templateUrl: 'views/users-group.html',
      restrict: 'E'
    };
  });
