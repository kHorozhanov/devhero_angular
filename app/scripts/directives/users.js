'use strict';

/**
 * @ngdoc directive
 * @name devheroApp.directive:users
 * @description
 * # users
 */
angular.module('devheroApp')
  .directive('users', function () {
    return {
      templateUrl: 'views/users.html',
      controller: 'UsersCtrl',
      restrict: 'E'
    };
  });
