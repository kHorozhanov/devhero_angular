'use strict';

/**
 * @ngdoc directive
 * @name devheroApp.directive:user
 * @description
 * # user
 */
angular.module('devheroApp')
  .directive('user', function () {
    return {
      templateUrl: 'views/user.html',
      restrict: 'E'
    };
  });
